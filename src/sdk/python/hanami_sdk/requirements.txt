jsonschema==4.23.0
protobuf==3.20.3
requests==2.32.3
simplejson==3.20.1
websockets==15.0.1
